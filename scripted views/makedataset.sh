#!/usr/bin/env bash

# Positional Parameter = name of dataset to store views

# Make sure the positional parameter matches the placeholder

bq mk $1_scripted_views

# cust_dbview_az
bq mk --use_legacy_sql=true --view='SELECT
    COUNT(tenantId) as total_activities_count,
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    "PROD" as tenantEnv
          
FROM 
  TABLE_DATE_RANGE([monitoring-analytics-694:monitoring_test.361z],
  DATE_ADD(CURRENT_TIMESTAMP(), -10, "DAY"), CURRENT_TIMESTAMP())

WHERE 
    TIME IS NOT NULL AND TIME != 0 AND customer = "placeholder"
GROUP BY
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    tenantEnv
  
ORDER BY
    total_activities_count DESC' $1_scripted_views.custdb_envs_361z

bq mk --use_legacy_sql=true --view='SELECT
    COUNT(tenantId) as total_activities_count,
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    "approd-01" as tenantEnv
       
    
FROM 
  TABLE_DATE_RANGE([monitoring-analytics-694:monitoring_test.approd01z],
  DATE_ADD(CURRENT_TIMESTAMP(), -10, "DAY"), CURRENT_TIMESTAMP())

WHERE 
    TIME IS NOT NULL AND TIME != 0 AND customer = "placeholder"

GROUP BY
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    tenantEnv
  
ORDER BY
    total_activities_count DESC' $1_scripted_views.custdb_envs_approd01z

bq mk --use_legacy_sql=true --view='SELECT
    COUNT(tenantId) as total_activities_count,
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    "euprod-01" as tenantEnv
       
    
FROM 
  TABLE_DATE_RANGE([monitoring-analytics-694:monitoring_test.euprod01z],
  DATE_ADD(CURRENT_TIMESTAMP(), -10, "DAY"), CURRENT_TIMESTAMP())
WHERE 
    TIME IS NOT NULL AND TIME != 0 AND customer = "placeholder"

GROUP BY
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    tenantEnv
  
ORDER BY
    total_activities_count DESC' $1_scripted_views.custdb_envs_euprod01z

bq mk --use_legacy_sql=true --view='SELECT
    COUNT(tenantId) as total_activities_count,
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    "mpe-01 (Europe)" as tenantEnv
       
    
FROM 
  TABLE_DATE_RANGE([monitoring-analytics-694:monitoring_test.mpe01z],
  DATE_ADD(CURRENT_TIMESTAMP(), -10, "DAY"), CURRENT_TIMESTAMP())
WHERE 
    TIME IS NOT NULL AND TIME != 0 AND customer = "placeholder"

GROUP BY
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    tenantEnv
  
ORDER BY
    total_activities_count DESC' $1_scripted_views.custdb_envs_mpe01z

bq mk --use_legacy_sql=true --view='SELECT
    COUNT(tenantId) as total_activities_count,
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    "mpe-03 (CarMax)" as tenantEnv
       
    
FROM 
  TABLE_DATE_RANGE([monitoring-analytics-694:monitoring_test.mpe03z],
  DATE_ADD(CURRENT_TIMESTAMP(), -10, "DAY"), CURRENT_TIMESTAMP())

WHERE 
    TIME IS NOT NULL AND TIME != 0 AND customer = "placeholder"

GROUP BY
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType,
    tenantEnv
  
ORDER BY
    total_activities_count DESC' $1_scripted_views.custdb_envs_mpe03z


bq mk --use_legacy_sql=true --view='SELECT
    COUNT(tenantId) as total_activities_count,
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    events.eventType as eventType,
    tenantEnv
           
FROM 
[monitoring-analytics-694:placeholder_scripted_views.custdb_envs_361z],
[monitoring-analytics-694:placeholder_scripted_views.custdb_envs_approd01z],
[monitoring-analytics-694:placeholder_scripted_views.custdb_envs_euprod01z],
[monitoring-analytics-694:placeholder_scripted_views.custdb_envs_mpe01z],
[monitoring-analytics-694:placeholder_scripted_views.custdb_envs_mpe03z]
   
WHERE 
    TIME IS NOT NULL AND TIME != 0 AND customer = "placeholder"

GROUP BY
    tenantId,
    TIME,
    user,
    customer,
    sourceSystem,
    eventType,
    tenantEnv
  
ORDER BY
    total_activities_count DESC' $1_scripted_views.custdb_envs

bq mk --use_legacy_sql=true --view='SELECT
    customerName,
    tenantName,
    tenantEnv,
    tenantId
FROM [monitoring-analytics-694:customer_success.Reltio360_TenantList]' $1_scripted_views.custdb_TenantList

# This commands is here so a dataset can be created to hold the main view
# to prevent the customer from directly querying the source datasets.

# bq mk $1_Dashboard_View

bq mk --use_legacy_sql=true --view='SELECT
  a.total_activities_count total_activities_count, 
  a.tenantId tenantId,
  a.TIME TIME, 
  a.user user, 
  a.sourceSystem sourceSystem,
  a.eventType eventType,
  b.tenantName tenantName, 
  b.customerName customerName, 
  b.tenantEnv tenantEnv

FROM
  [monitoring-analytics-694:placeholder_scripted_views.custdb_envs] a

JOIN [monitoring-analytics-694:placeholder_scripted_views.custdb_TenantList] b 
  ON a.tenantId=b.tenantId' $1_scripted_views.customer_view


