#!/usr/bin/env bash

# Positional Parameter 1 = Dataset that Holds Dashboard Views
# This script should only be used when you have modified the access control information in the JSON files
# and want to see the changes reflected in BigQuery

bq update --source=$1/$1_Customer_View.json $1_Customer_View
bq --format=prettyjson show $1_Customer_View >$1/updated_$1_Customer_View.json
bq update --source=$1/$1_Dashboard.json $1_Dashboard
bq --format=prettyjson show $1_Dashboard >$1/updated_$1_Dashboard.json

