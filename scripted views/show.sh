#!/usr/bin/env bash

# Positional Parameter 1 - Customer Name for Dataset Holding Dashboard Views
# This script downloads the access control information for the dataset created in makedataset.sh
# The JSON file is then placed in the folder named after the positonal parameter.

bq --format=prettyjson show $1_scripted_views >$1/$1_scripted_views.json
# bq --format=prettyjson show $1_Customer_View >$1/$1_Customer_View.json
