#!/usr/bin/env bash

# Positional Parameter 1 - Customer Name for Dataset Holding Dashboard Views
# Make sure the positional parameter matches the company name in the sed command.

[ -d $1 ] || mkdir $1

# This can be written as sed -i .bak 's/placeholder/AstraZeneca/'
# This replaces all of the placeholders in views within makedataset.sh with the name of the company.
sed -i .bak 's/placeholder/company_name/' makedataset.sh

# Creates the Views for the Customer Dashboard within BigQuery
./makedataset.sh $1

# Downloads a JSON file containing the access-control information for the datasets.
./show.sh $1

